import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { StatisticsInfoService } from "~/home/services/statistics-info.service";
import { Statistics } from "~/home/models/statistics";
import { HomeService } from "~/home/services/home.services";
import { Color } from "tns-core-modules/color/color";

@Component({
    selector: "Statistics-grade",
    moduleId: module.id,
    templateUrl: "./statistics-grade.component.html",
    styleUrls: ["./statistics-grade.component.scss"]
})
export class StatisticsGradeComponent implements OnInit {
    grades = [1, 2, 3, 4, 5];
    selectedGrade = null;
    constructor(
        private routerExtensions: RouterExtensions,
        private statisticsInfoService: StatisticsInfoService,
        private homeService: HomeService
    ) {}

    ngOnInit(): void {
    }

    finished(): void {
        this.statisticsInfoService.setGrade(this.selectedGrade);
        this.homeService.setStatistics(this.statisticsInfoService.getStatistics()).subscribe((element) => {
            this.routerExtensions.navigateByUrl("/statistics-players");
        });
    }

    itemTap(event: any) {
        let gridLayout = event.view;
        this.selectedGrade = this.grades[event.index];

        gridLayout.backgroundColor = new Color("#DCEDC8");
    }

    finishedSet(): void {
        this.routerExtensions.navigateByUrl("/finished-set");
    }

    onNavBtnTap(){
        this.routerExtensions.navigateByUrl("/statistics-element");
    }
}

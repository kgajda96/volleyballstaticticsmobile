import { Component, OnInit } from "@angular/core";
import { MatchPlayers } from "~/home/models/match-players";
import { MatchInfo } from "~/home/models/match-info";
import { TextField } from "tns-core-modules/ui/text-field";
import { RouterExtensions } from "nativescript-angular/router";
import { HomeService } from "~/home/services/home.services";
import { UserInfoService } from "~/home/services/user-info.services";
import { LoginModel } from "~/home/models/login.model";
import { TeamModel } from "~/home/models/team.model";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatchInfoService } from "~/home/services/match-info.services";
import { MatchBaseInfo } from "~/home/models/match-base-info";

@Component({
    selector: "Match-info",
    moduleId: module.id,
    templateUrl: "./match-info.component.html",
    styleUrls: ["./match-info.component.scss"]
})
export class MatchInfoComponent implements OnInit {
    teams: TeamModel[];
    teamsName: string[];
    selectedIndex: number;
    matchForm: FormGroup;
    loginUser: string;
    startDate = new Date();
    matchDate = new Date();
    constructor(
        private routerExtensions: RouterExtensions,
        private homeService: HomeService,
        private userInfoService: UserInfoService,
        private formBuilder: FormBuilder,
        private matchInfoService: MatchInfoService
    ) {}

    ngOnInit(): void {
        this.matchForm = this.formBuilder.group({
            date: [new Date(), Validators.required],
            matchOpponnent: ["", Validators.required]
        });

        this.userInfoService.userInfo$.subscribe((userInfo: LoginModel) => {
            this.loginUser = userInfo.login;
            this.homeService.getTeams(userInfo.login).subscribe(teams => {
                this.teams = teams;
                this.teamsName = teams.map(team => team.name);
            });
        });
    }

    createMatch(): void {
        this.homeService.setMatch(this.prepareMatchModel()).subscribe(matchId => {
            this.matchInfoService.setMatchInfo(
                this.prepareMatchInfo(matchId, this.matchForm.controls.matchOpponnent.value)
            );
            this.routerExtensions.navigateByUrl("/create-set");
        });
    }

    onDateChanged(event: any) {
        this.matchDate = event.value;
        this.matchForm.controls.date.setValue(this.matchDate);
    }

    onSelectedIndexChanged(event: any) {
        this.selectedIndex = event.value;
        this.matchForm.controls.matchOpponnent.setValue(this.teams.find((item, index) => index === this.selectedIndex).name);
    }

    onNavBtnTap(){
        this.routerExtensions.navigateByUrl("/welcome");
    }

    private prepareMatchModel(): MatchInfo {
        return {
            date: this.matchForm.get("date").value,
            matchOpponnent: this.matchForm.get("matchOpponnent").value,
            loginUser: this.loginUser
        };
    }

    private prepareMatchInfo(matchId: number, matchOpponnent: string): MatchBaseInfo {
        return {
            matchId,
            matchOpponnent
        };
    }
}

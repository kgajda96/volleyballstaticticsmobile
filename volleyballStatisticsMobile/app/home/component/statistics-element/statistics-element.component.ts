import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { HomeService } from "~/home/services/home.services";
import { Color } from "tns-core-modules/color/color";
import { StatisticsInfoService } from "~/home/services/statistics-info.service";
import { Element } from '~/home/models/element.model';

@Component({
    selector: "Statistics-element",
    moduleId: module.id,
    templateUrl: "./statistics-element.component.html",
    styleUrls: ["./statistics-element.component.scss"]
})
export class StatisticsElementComponent implements OnInit {
    elements: Element[];
    selectedElement: Element;
    constructor(private routerExtensions: RouterExtensions, private homeService: HomeService, private statisticsInfoService: StatisticsInfoService) {
    }

    ngOnInit(): void {
        this.homeService.getElement().subscribe((elements: Element[]) => {
            this.elements = elements;
        })
    }
    
    onNavBtnTap(){
        this.routerExtensions.navigateByUrl("/statistics-players");
    }
    
    itemTap(event: any) {
        let gridLayout = event.view;
        this.selectedElement = this.elements[event.index];

        gridLayout.backgroundColor = new Color("#DCEDC8");
    }

    finished(): void {
        this.statisticsInfoService.setElement(this.selectedElement.name);
        this.routerExtensions.navigateByUrl('/statistics-grade');
    }

    finishedSet(): void {
        this.routerExtensions.navigateByUrl("/finished-set");
    }
}

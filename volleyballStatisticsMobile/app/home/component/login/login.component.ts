import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { LoginService } from "~/home/services/login.services";
import { UserInfoService } from "~/home/services/user-info.services";
import { LoginModel } from "~/home/models/login.model";

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;

    constructor(
        private routerExtensions: RouterExtensions,
        private formBuilder: FormBuilder,
        private loginService: LoginService,
        private userInfoService: UserInfoService
    ) {}

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            login: ["", Validators.required],
            password: ["", Validators.required]
        });
    }

    login(): void {
        this.loginService
            .loginUser(this.loginForm.value)
            .subscribe((team: any) => {
                this.userInfoService.setUserInfo(this.prepareUserInfo(this.loginForm.value, team.Name));
                this.routerExtensions.navigateByUrl("/welcome");
            });
    }

    onNavBtnTap(){
        this.routerExtensions.navigateByUrl("/home");
    }

    prepareUserInfo(loginForm: any, teamName: string): LoginModel {
        return {
            login: loginForm.login,
            password: '',
            teamName: teamName
        }
    }
}

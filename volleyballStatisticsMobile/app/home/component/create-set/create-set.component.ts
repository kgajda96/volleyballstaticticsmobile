import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { HomeService } from "~/home/services/home.services";
import { SetInfoService } from "~/home/services/set-info.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SetInfo } from "~/home/models/set-info";
import { MatchInfoService } from "~/home/services/match-info.services";
import { MatchBaseInfo } from "~/home/models/match-base-info";
import { StatisticsInfoService } from "~/home/services/statistics-info.service";

@Component({
    selector: "Create-set",
    moduleId: module.id,
    templateUrl: "./create-set.component.html",
    styleUrls: ["./create-set.component.scss"]
})
export class CreateSetComponent implements OnInit {
    setForm: FormGroup;
    matchId: number;

    constructor(
        private routerExtensions: RouterExtensions,
        private homeService: HomeService,
        private setInfoService: SetInfoService,
        private formBuilder: FormBuilder,
        private matchInfoService: MatchInfoService,
        private statisticsInfoService: StatisticsInfoService
    ) {}

    ngOnInit(): void {
        this.setForm = this.formBuilder.group({
            number: [null, Validators.required],
        })

        this.matchInfoService.getMatchInfo().subscribe((matchInfo: MatchBaseInfo) => {
            this.matchId = matchInfo.matchId;
        });
    }

    onNavBtnTap(){
        this.routerExtensions.navigateByUrl("/match-info");
    }

    private prepareSetInfo(): SetInfo {
        return {
            setNumber: parseInt(this.setForm.controls.number.value),
            matchId: this.matchId,
            team1Result: 0,
            team2Result: 0
        }
    }

    createSet(): void {
        this.homeService.setSet(this.prepareSetInfo()).subscribe((setId) => {
            this.setInfoService.setSetInfo({setId: setId, number: this.setForm.get('number').value} );
            this.routerExtensions.navigateByUrl("/statistics-players");
        })
    }
}

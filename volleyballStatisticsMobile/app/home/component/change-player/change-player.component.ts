import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "Change-player",
    moduleId: module.id,
    templateUrl: "./change-player.component.html",
    styleUrls: ["./change-player.component.scss"]
})
export class ChangePlayerComponent implements OnInit {
    
    constructor(private routerExtensions: RouterExtensions) {
    }

    ngOnInit(): void {

    }

    changePlayer(): void {
        this.routerExtensions.navigateByUrl('/statistics');
    }
}

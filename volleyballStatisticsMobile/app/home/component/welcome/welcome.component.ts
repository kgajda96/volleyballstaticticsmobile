import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { UserInfoService } from "~/home/services/user-info.services";
@Component({
    selector: "Welcome",
    moduleId: module.id,
    templateUrl: "./welcome.component.html",
    styleUrls: ["./welcome.component.css"]
})
export class WelcomeComponent implements OnInit {

    welcomeLabel: string;
    teamLabel: string;
    constructor(private routerExtensions: RouterExtensions, private userInfoService: UserInfoService) {
    }

    ngOnInit(): void {
        this.teamLabel = "Twój zespół to NAZWA_ZESPOŁU";
        this.welcomeLabel = "Witaj admin";
    }

    createMatch(): void {
        this.routerExtensions.navigateByUrl("/match-info");
    }

    onNavBtnTap(args: any){
        this.routerExtensions.navigateByUrl("/login");
    }
}

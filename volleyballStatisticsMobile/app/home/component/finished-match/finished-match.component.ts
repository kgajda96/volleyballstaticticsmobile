import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "Finished-match",
    moduleId: module.id,
    templateUrl: "./finished-match.component.html",
    styleUrls: ["./finished-match.component.scss"]
})
export class FinishedMatchComponent implements OnInit {
    
    constructor(private routerExtensions: RouterExtensions) {
    }

    ngOnInit(): void {

    }

    createMatch(): void {
        this.routerExtensions.navigateByUrl('/match-info');
    }

    onNavBtnTap(){
        this.routerExtensions.navigateByUrl("/finished-set");
    }
}

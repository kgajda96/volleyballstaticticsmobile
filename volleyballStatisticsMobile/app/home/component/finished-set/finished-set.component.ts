import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { HomeService } from "~/home/services/home.services";
import { MatchInfoService } from "~/home/services/match-info.services";
import { SetInfoService } from "~/home/services/set-info.service";
import { zip } from "rxjs";
import { SetInfo } from "~/home/models/set-info";
import { FormGroup, FormBuilder } from "@angular/forms";
import { MatchResultService } from "~/home/services/match-result.services";
import { MatchResult } from "~/home/models/match-result";

@Component({
    selector: "Finished-set",
    moduleId: module.id,
    templateUrl: "./finished-set.component.html",
    styleUrls: ["./finished-set.component.scss"]
})
export class FinishedSetComponent implements OnInit {
    finishedSetForm: FormGroup;

    constructor(
        private routerExtensions: RouterExtensions,
        private homeSerivce: HomeService,
        private matchInfoService: MatchInfoService,
        private setInfoService: SetInfoService,
        private formBuilder: FormBuilder,
        private matchResultService: MatchResultService
    ) {}

    ngOnInit(): void {
        this.finishedSetForm = this.formBuilder.group({
            team1Result: [null],
            team2Result: [null]
        })
    }

    finishedMatch(): void {
        this.saveSetResult("/finished-match");
    }

    finishedSet(): void {
        this.saveSetResult("/create-set");
    }

    onNavBtnTap(){
        this.routerExtensions.navigateByUrl("/statistics-players");
    }

    private saveSetResult(routing: string): void {
        zip(this.matchInfoService.matchInfo$, this.setInfoService.setInfo$).subscribe(([matchInfo, setInfo]) => {
            this.homeSerivce.finishedSet(this.prepareSetInfo(matchInfo.matchId, setInfo.number), setInfo.setId).subscribe(() => {
                this.routerExtensions.navigateByUrl(routing);
            });
        });
    }

    private prepareSetInfo(matchId: number, setNumber: any): SetInfo {
        return {
            matchId,
            team1Result: parseInt(this.finishedSetForm.get('team1Result').value),
            team2Result: parseInt(this.finishedSetForm.get('team2Result').value),
            setNumber: parseInt(setNumber)
        };
    }
}

import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { HomeService } from "~/home/services/home.services";
import { UserInfoService } from "~/home/services/user-info.services";
import { LoginModel } from "~/home/models/login.model";
import { MatchPlayers } from "~/home/models/match-players";
import { Color } from "tns-core-modules/color/color";
import { StatisticsInfoService } from "~/home/services/statistics-info.service";

@Component({
    selector: "Statistics-players",
    moduleId: module.id,
    templateUrl: "./statistics-players.component.html",
    styleUrls: ["./statistics-players.component.scss"]
})
export class StatisticsPlayersComponent implements OnInit {
    players: MatchPlayers[];
    selectedPlayers: MatchPlayers[] = [];
    backgroundColor = [];
    constructor(
        private routerExtensions: RouterExtensions,
        private homeService: HomeService,
        private userInfoService: UserInfoService,
        private statisticsInfoService: StatisticsInfoService
    ) {}

    ngOnInit(): void {
        this.statisticsInfoService.setStatistics();
        this.userInfoService.getUserInfo().subscribe((userInfo: LoginModel) => {
            this.homeService.getPlayers(userInfo.login).subscribe((players: MatchPlayers[]) => {
                this.players = players;
            })
        })
    }

    finished(): void {
        this.statisticsInfoService.setPlayers(this.selectedPlayers);
        this.routerExtensions.navigateByUrl("/statistics-element");
    }

    itemTap(event: any) {
        let gridLayout = event.view;
        this.selectedPlayers.push(this.players.find((element, index) => {
            return index == event.index;
        }));

        gridLayout.backgroundColor = new Color("#DCEDC8");
    }

    finishedSet(): void {
        this.routerExtensions.navigateByUrl("/finished-set");
    }

    onNavBtnTap(){
        this.routerExtensions.navigateByUrl("/create-set");
    }
}

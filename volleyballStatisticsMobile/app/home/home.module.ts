import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptUICalendarModule } from "nativescript-ui-calendar/angular";
import { NativeScriptUIChartModule } from "nativescript-ui-chart/angular";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";
import { NativeScriptUIAutoCompleteTextViewModule } from "nativescript-ui-autocomplete/angular";
import { NativeScriptUIGaugeModule } from "nativescript-ui-gauge/angular";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./component/home/home.component";
import { LoginComponent } from "./component/login/login.component";
import { MatchInfoComponent } from "./component/match-info/match-info.component";
import { CreateSetComponent } from "./component/create-set/create-set.component";
import { FinishedMatchComponent } from "./component/finished-match/finished-match.component";
import { FinishedSetComponent } from "./component/finished-set/finished-set.component";
import { ChangePlayerComponent } from "./component/change-player/change-player.component";
import { StatisticsGradeComponent } from "./component/statistics-grade/statistics-grade.component";
import { ReactiveFormsModule } from "@angular/forms";
import { LoginService } from "./services/login.services";
import { HomeService } from "./services/home.services";
import { UserInfoService } from "./services/user-info.services";
import { MatchInfoService } from "./services/match-info.services";
import { SetInfoService } from "./services/set-info.service";
import { StatisticsPlayersComponent } from "./component/statistics-players/statistics-players.component";4
import { StatisticsElementComponent } from "./component/statistics-element/statistics-element.component";
import { StatisticsInfoService } from "./services/statistics-info.service";
import { MatchResultService } from "./services/match-result.services";
import { WelcomeComponent } from "./component/welcome/welcome.component";

@NgModule({
    imports: [
        NativeScriptUISideDrawerModule,
        NativeScriptUIListViewModule,
        NativeScriptUICalendarModule,
        NativeScriptUIChartModule,
        NativeScriptUIDataFormModule,
        NativeScriptUIAutoCompleteTextViewModule,
        NativeScriptUIGaugeModule,
        NativeScriptCommonModule,
        HomeRoutingModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        ReactiveFormsModule
    ],
    declarations: [
        HomeComponent,
        LoginComponent,
        MatchInfoComponent,
        CreateSetComponent,
        FinishedMatchComponent,
        FinishedSetComponent,
        ChangePlayerComponent,
        StatisticsGradeComponent,
        StatisticsElementComponent,
        StatisticsPlayersComponent,
        WelcomeComponent
    ],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [LoginService, HomeService, UserInfoService, MatchInfoService, SetInfoService, StatisticsInfoService, MatchResultService]
})
export class HomeModule {}

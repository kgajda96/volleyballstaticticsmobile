import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { HomeComponent } from "./component/home/home.component";
import { LoginComponent } from "./component/login/login.component";
import { MatchInfoComponent } from "./component/match-info/match-info.component";
import { CreateSetComponent } from "./component/create-set/create-set.component";
import { FinishedSetComponent } from "./component/finished-set/finished-set.component";
import { FinishedMatchComponent } from "./component/finished-match/finished-match.component";
import { ChangePlayerComponent } from "./component/change-player/change-player.component";
import { StatisticsGradeComponent } from "./component/statistics-grade/statistics-grade.component";
import { StatisticsElementComponent } from "./component/statistics-element/statistics-element.component";
import { StatisticsPlayersComponent } from "./component/statistics-players/statistics-players.component";
import { WelcomeComponent } from "./component/welcome/welcome.component";

const routes: Routes = [
    {
        path: "", component: HomeComponent, pathMatch: 'full'
    },
    {
        path: "login",
        component: LoginComponent
    },
    {
        path: "match-info",
        component: MatchInfoComponent
    },
    {
        path: "create-set",
        component: CreateSetComponent
    },
    {
        path: "finished-set",
        component: FinishedSetComponent
    },
    {
        path: "finished-match",
        component: FinishedMatchComponent
    },
    {
        path: "change-player",
        component: ChangePlayerComponent
    },
    {
        path: "statistics-grade",
        component: StatisticsGradeComponent
    },
    {
        path: "statistics-element",
        component: StatisticsElementComponent
    },
    {
        path: "statistics-players",
        component: StatisticsPlayersComponent
    },
    {
        path: "welcome",
        component: WelcomeComponent
    },
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class HomeRoutingModule { }

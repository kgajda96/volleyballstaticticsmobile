export interface SetInfo {
    matchId: number;
    setNumber: number;
    team1Result: number;
    team2Result: number;
}
import { MatchPlayers } from "./match-players";

export interface MatchInfo {
    date: string,
    matchOpponnent: string,
    loginUser: string
}
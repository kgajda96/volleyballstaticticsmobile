import { MatchPlayers } from "./match-players";

export interface Statistics {
    setId: number;
    gradeNumber: number;
    elementName: string;
    playersIdsList: number[];
}
export interface MatchResult {
    team1Result: number;
    team2Result: number;
}
export interface MatchBaseInfo {
    matchId: number;
    matchOpponnent: string;
}
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { LoginModel } from "../models/login.model";
import { MatchBaseInfo } from "../models/match-base-info";

@Injectable()
export class MatchInfoService {
    matchInfoDefault: MatchBaseInfo = {
        matchId: null,
        matchOpponnent: ''
    };
    matchInfo$ = new BehaviorSubject<MatchBaseInfo>(this.matchInfoDefault);

    setMatchInfo(matchInfo: MatchBaseInfo): void {
        this.matchInfo$.next(matchInfo);
    }

    getMatchInfo(): Observable<MatchBaseInfo> {
        return this.matchInfo$;
    }
}
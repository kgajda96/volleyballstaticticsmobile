import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ApiUrl } from "./api-url.const";
import { Observable } from "rxjs";
import { MatchInfo } from "../models/match-info";
import { SetInfo } from "../models/set-info";
import { Statistics } from "../models/statistics";
import { SetBaseInfo } from "../models/set-base-info";

@Injectable()
export class HomeService {
    private serverUrl = ApiUrl;

    constructor(private http: HttpClient) { }

    getTeams(login: string): Observable<any> {
        return this.http.get(this.serverUrl + 'team/' + login);
    }

    setMatch(matchInfo: MatchInfo): Observable<any> {
        return this.http.post(this.serverUrl + 'match', matchInfo)
    }

    setSet(setInfo: SetInfo): Observable<any> {
        return this.http.post(this.serverUrl + 'set', setInfo);
    }

    getPlayers(login: string): Observable<any> {
        return this.http.get(this.serverUrl + 'players/' + login);
    }

    getElement(): Observable<any> {
        return this.http.get(this.serverUrl + 'elements');
    }

    setStatistics(statistics: Statistics): Observable<any> {
        return this.http.post(this.serverUrl + 'statistics', statistics);
    }

    
    finishedSet(setInfo: SetInfo, setId: number): Observable<any> {
        return this.http.post(this.serverUrl + 'set/' + setId, setInfo);
    }
}
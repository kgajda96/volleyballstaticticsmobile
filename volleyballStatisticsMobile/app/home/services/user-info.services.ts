import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { LoginModel } from "../models/login.model";

@Injectable()
export class UserInfoService {
    userInfoDefault: LoginModel = {
        login: '',
        password: '',
        teamName: ''
    }

    userInfo: LoginModel;
    userInfo$ = new BehaviorSubject<LoginModel>(this.userInfoDefault);

    setUserInfo(userInfo: LoginModel): void {
        this.userInfo$.next(userInfo);
        this.userInfo = userInfo;
    }

    getUserInfo(): Observable<LoginModel> {
        return this.userInfo$;
    }
}
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { LoginModel } from "../models/login.model";
import { Observable } from "rxjs";
import { ApiUrl } from "./api-url.const";

@Injectable()
export class LoginService {
    private serverUrl = ApiUrl;

    constructor(private http: HttpClient) { }

    loginUser(userInfo: LoginModel): Observable<any> {
        return this.http.post(this.serverUrl + 'users/check', userInfo);
    }
}
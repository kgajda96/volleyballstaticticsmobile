import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { MatchResult } from "../models/match-result";

@Injectable()
export class MatchResultService {
    matchResultDefault: MatchResult = {
        team1Result: null,
        team2Result: null
    };
    matchResult$ = new BehaviorSubject<MatchResult>(this.matchResultDefault);

    setMatchResult(matchResult: MatchResult): void {
        this.matchResult$.next(matchResult);
    }

    getMatchResult(): Observable<MatchResult> {
        return this.matchResult$;
    }
}
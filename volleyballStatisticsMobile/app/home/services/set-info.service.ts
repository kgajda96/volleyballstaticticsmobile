import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { SetBaseInfo } from "../models/set-base-info";

@Injectable()
export class SetInfoService {
    setInfoDefault: SetBaseInfo = {
        setId: null,
        number: null
    };
    public setId: number;
    setInfo$ = new BehaviorSubject<SetBaseInfo>(this.setInfoDefault);

    setSetInfo(SetInfo: SetBaseInfo): void {
        this.setInfo$.next(SetInfo);
        this.setId = SetInfo.setId;
    }

    getSetInfo(): Observable<SetBaseInfo> {
        return this.setInfo$;
    }
}
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, forkJoin, zip } from "rxjs";
import { Statistics } from "../models/statistics";
import { MatchPlayers } from "../models/match-players";
import { SetInfoService } from "./set-info.service";

@Injectable()
export class StatisticsInfoService {
    statisticsDefault: Statistics = {
        setId: null,
        playersIdsList: [],
        gradeNumber: null,
        elementName: '',
    };
    statistics = this.statisticsDefault;
    players$ = new BehaviorSubject<MatchPlayers[]>([]);
    grade$ = new BehaviorSubject<number>(null);
    element$ = new BehaviorSubject<string>('');

    constructor(private setInfoService: SetInfoService) {}
    setPlayers(matchPlayers: MatchPlayers[]): void {
        this.players$.next(matchPlayers);
    }

    setGrade(grade: number): void {
        this.grade$.next(grade);
    }

    setElement(element: string): void {
        this.element$.next(element);
    }

    setStatistics(): void {
        zip(this.players$, this.grade$, this.element$)
        .subscribe(([players, grade, element]) => {
            this.statistics = {
                playersIdsList: players.map((e) => e.id),
                gradeNumber: grade,
                elementName: element,
                setId: this.setInfoService.setInfo$.value.setId
            }
        })
    }

    getStatistics(): Statistics {
        return this.statistics;
    }
}